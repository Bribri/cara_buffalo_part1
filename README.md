# Installation

Buffalo est un ecosystème permettant d'assembler des packages Go afin de créer une application Web rapidement. Il vous faudra donc installer d'abord Go, éventuellement Sqlite (Windows) et seulement ensuite Buffalo. Vous trouverez ci-dessous un récapitulatif des instructions d'installation.

## Sous Linux 

1. Executer le script d'installation de Buffalo

2. Se positionner dans le répertoire C:\Users\<username>\go\src\<user>

3. Créer un nouveau projet en saisissant la ligne de commande `$ buffalo new my_app --db-type sqlite3`


## Sous Windows

1. Vérifier que votre version de Node >= 8.0.0

1. Installer Go (cf. https://www.gopherguides.com/courses/preparing-your-environment-for-go-development/modules/setting-up-windows-module/) 

2. Installer le GCC (cf. https://www.gopherguides.com/courses/preparing-your-environment-for-go-development/modules/setting-up-windows-module/#slide-24)

3. Installer Sqlite (cf. https://www.gopherguides.com/courses/preparing-your-environment-for-go-development/modules/setting-up-windows-module/#slide-27)

4. Installer Buffalo en saisissant la ligne de commande `$ go get -u -v -tags sqlite github.com/gobuffalo/buffalo/buffalo`


# INTRODUCTION

Lors de cette séance, nous allons créer une gestion d'utilisateurs pour un site Web (enregistrement, login et logout).


# PARTIE 1 - From scratch

## 1.1 - Générer un nouveau projet

La puissance de Buffalo réside dans sa capacité à assembler rapidement tous les packages nécessaires pour obtenir une application avec un front et un back-end fonctionnels et pré-câblés.

Dans cette première partie, nous allons générer un nouveau projet et créer une entité Users via l'outil en ligne de commande Buffalo.

1. Se positionner dans le répertoire C:\Users\<username>\go\src\<user>

2. Créer un nouveau projet en lançant `$ buffalo new my_app --db-type sqlite3`

3. Une fois le projet généré, vérifiez la configuration de votre base de données. Les paths spécifiés dans le fichier database.yml correspondront aux bases créées par Buffalo.

4. En vous plaçant à la racine de votre projet my_app et en lançant la commande `buffalo`, vous pourrez consulter l'aide de Buffalo, qui sera utile pour la suite.

5. Vous pouvez désormais lancer votre projet via la commande `$ buffalo dev` et accéder à votre projet sur `localhost:3000`


NB : pour vérifier les routes et leurs noms tout au long du projet, saisir la commande `$ buffalo routes`


## 1.2 - Créer une entité, sa ressource et la migration DB

Avant de créer notre entité User, il faut créer les bases de données avec la commande `$ buffalo db create -a`.

Par la suite, pour visualiser le statut des migrations, vous pourrez utiliser `$ buffalo db migrate status`.

Buffalo met à disposition une liste de generateurs via la commande `$ buffalo generate` (voir buffalo generate -help pour plus d'infos) permettant de monter son projet plus rapidement.
Ainsi, il permet de créer en une seule ligne de commande (buffalo generate resource) une entité, la migration BDD, l'html et les ressources (CRUD Endpoints) correspondants.


Saisissez `$ buffalo g r users username email admin:boolean`pour créer l'entité User.

Vous pourrez vérifer que les fichiers ci-dessous ont été créés par Buffalo :

+ actions/users.go : les ressources associées aux Users
+ actions/app.go : le mapping `app.Resource("/users", UsersResource{})` a bien été ajouté
+ models/user.go : le modèle User contient une struct contenant les champs Username, Email et Admin
+ migrations/aaaammjj_create_users.up.fizz : la migration de la table Users en BDD a été créée
+ templates/users : les fichiers html de création, consultation, édition du modèle User ont été créés


En vous rendant sur l'url `http://localhost:3000/users/new`, vous pouvez vérifier que le formulaire de création d'un utilisateur a été généré automatiquement. 

Vérifiez le statut de la migration "create_users", elle doit être en "Pending".

Recherchez dans l'aide de Buffalo ($ buffalo db -h) la commande permettant d'appliquer cette migration. Elle doit passer en statut "Applied" et générer la création d'un fichier `schema.sql`sous le répertoire `migrations`.





